# Variables are documented in hack/build.sh.
BASE_IMAGE_NAME = luigi
VERSIONS = 2.7
OPENSHIFT_NAMESPACES = 2.7

.PHONY: $(shell test -f common/common.mk || echo >&2 'Please do "git submodule update --init" first.')

include common/common.mk